/*
This code currently sends a json string to an address in five minute intervals 
using the delay function.
*/


#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <dht.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#define dht_apin 0

int soil,soil_pin = 2,temp,humidity,sensor_id = 1,farm_id = 1;
unsigned long epochtime,t;

const char* ssid = "";        //add wifi information in here
const char* password = "";

WiFiUDP ntpUDP;
dht DHT;
HTTPClient http;


NTPClient timeClient(ntpUDP, "time.nist.gov", 0 , 60000);        //unix timestamp server


void setup() {

  Serial.setTimeout(30000);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {

  delay(500);
  Serial.println("Waiting for connection...");
    
  }

  timeClient.begin();

}

void loop() {

 t = millis();
do
{

  if(WiFi.status() == WL_CONNECTED){

  read_sensors();
  timeClient.update();
  epochtime = timeClient.getEpochTime();

  String senselog = "{\"farm_id\":\"1\",\"timestamp\":\"";     //creation of the json format string
  senselog += epochtime;
  senselog += "\",\"temp\":\"";
  senselog += temp;
  senselog += "\",\"humidity\":\"";
  senselog += humidity;
  senselog += "\",\"soil\":\"";
  senselog += soil;
  senselog += "\"}";


  http.begin("http://arcane-springs-61937.herokuapp.com/apiv1/farms/1/senselogs");    //address the values are being sent to
  http.addHeader("Content-Type", "application/json");
  
  int httpCode = http.POST(senselog);
  String payload = http.getString();


  Serial.println(senselog);
  Serial.println(payload);

  http.end();
  
  delay(500000);
    
  }else{

    Serial.println("WiFi Error");
  }
}while(t%500000 == 0);
  

}
void read_sensors()
{
   DHT.read11(dht_apin);

   humidity = DHT.humidity;
   temp = DHT.temperature;

   soil = analogRead(soil_pin);
   soil = map(soil,550,0,0,100);
}
